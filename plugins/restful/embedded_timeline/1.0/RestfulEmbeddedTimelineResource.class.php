<?php

/**
 * @file
 * Class RestfulEmbeddedTimelineResource.
 */

/**
 *
 */
class RestfulEmbeddedTimelineResource extends RestfulEntityBaseNode {

  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();
    $public_fields['timeline'] = array('property' => 'field_timeline_collection');
    return $public_fields;
  }

}
