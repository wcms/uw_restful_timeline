<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('Embedded Timeline'),
  'resource' => 'embedded_timeline',
  'name' => 'embedded_timeline__1_0',
  'entity_type' => 'node',
  'bundle' => 'uw_embedded_timeline',
  'description' => t('Export the information for embedded timeline content type.'),
  'class' => 'RestfulEmbeddedTimelineResource',
);
