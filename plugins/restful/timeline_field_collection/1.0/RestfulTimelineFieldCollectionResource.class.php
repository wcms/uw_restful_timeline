<?php

/**
 * @file
 * Class RestfulTimelineFieldCollectionResource.
 */

/**
 *
 */
class RestfulTimelineFieldCollectionResource extends RestfulEntityBase {

  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();
    $public_fields['content'] = array('property' => 'field_timeline_content', 'sub_property' => 'value');
    return $public_fields;
  }

}
