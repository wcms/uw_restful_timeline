<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('Timeline Field Collection'),
  'resource' => 'timeline_field_collection',
  'name' => 'timeline_field_collection__1_0',
  'entity_type' => 'field_collection',
  'bundle' => 'field_timeline_collection',
  'description' => t('Export the timeline field collection.'),
  'class' => 'RestfulTimelineFieldCollectionResource',
);
